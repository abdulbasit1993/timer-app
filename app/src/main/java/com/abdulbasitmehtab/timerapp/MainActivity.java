package com.abdulbasitmehtab.timerapp;

import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    MediaPlayer player;

    EditText editText;
    Button button, ringStop;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editText = (EditText) findViewById(R.id.editText);
        button = (Button) findViewById(R.id.button);
        textView = (TextView) findViewById(R.id.textView);
        ringStop = (Button) findViewById(R.id.btStopRing);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = editText.getText().toString();
                if(!text.equalsIgnoreCase("")) {
                    int seconds = Integer.valueOf(text) + 1;
                    CountDownTimer countDownTimer = new CountDownTimer(seconds * 1000, 1000) {
                        @Override
                        public void onTick(long millis) {
                            textView.setText("seconds: " + (int) (millis / 1000));
                        }

                        @Override
                        public void onFinish() {
                            textView.setText("Finished!");
                            Toast.makeText(MainActivity.this, "Timer is up!", Toast.LENGTH_LONG).show();
                            if(player == null) {
                                player = MediaPlayer.create(getApplicationContext(), R.raw.timer);
                            }
                            player.setLooping(true);
                            player.start();
                        }

                    }.start();
                }
            }
        });

        ringStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (player != null) {
                    player.release();
                    player = null;
                    Toast.makeText(MainActivity.this, "Ringing stopped!", Toast.LENGTH_LONG).show();
                }
            }
        });

    }

}